package ito.poo.app;

import java.util.Scanner;

public class MyApp {
	
	static Scanner input = new Scanner(System.in); 
	//****************************************
	static void leerTemp(float [] t) {
		for(int i=0;i<t.length;i++) {
			System.out.print("Proporciona temperatura:"+(i+1)+":");
			t[i]= input.nextFloat();
		}
	}
	//***************************************
	static float obtenerMayor(float [] t) {
		float mayor=t[0];
		for(int i=1;i<t.length;i++)
			if(t[i]>mayor)
				mayor=t[i];
		return mayor;
	}
	//****************************************
	static float obtenerMenor(float [] t) {
		float menor=t[0];
		for(int i=1;i<t.length;i++)
			if(t[i]<menor)
				menor=t[i];
		return menor;
	}
	//****************************************
	static void repetidos (float [] r,float [] t) {	
		for(int i=0;i<t.length;i++) {
            float x=t[i];		
			for(int j=i;j<t.length;j++)
				if(x==t[j])
					r[i]++;
		}
	}
	//****************************************
	static int cuentaMayor(float c[],float m) {
		int k=0;
		for(int i=0;i<c.length;i++)
			if(c[i]==m)
				k++;
		return k;
	}
	//********************************************
	static void llenaTempRepetidas(float r[],float c[],float t[],float m) {
		int k=0;
		for(int i=0;i<c.length;i++)
			if(c[i]==m) {
				r[k]=t[i];
				k++;
			}
	}
	//********************************************
    static float [] tempMasRepetidas(float [] t) {
    	float cuenta[] = new float[t.length];
    	repetidos(cuenta,t);
    	float m=obtenerMayor(cuenta);
    	float r[]=null;
    	if(m>1) {
    		r= new float[cuentaMayor(cuenta,m)];
    		llenaTempRepetidas(r,cuenta,t,m);
    	}
    	return r;
    }
    //*******************************************
    static float obtienePromedio(float [] t) {
    	float p=0;
    	for(int i=0;i<t.length;i++)
    		p+=t[i];
    	return p/t.length;
    }
    //*******************************************
    static void imprimeResultados(float [] t) {
    	System.out.printf("La temparatura mayor=%6.2f Grados Centigrados\n", obtenerMayor(t));
    	System.out.printf("La temparatura menor=%6.2f Grados Centigrados\n", obtenerMenor(t));
    	float r[]= tempMasRepetidas(t);
    	if(r == null)
    		System.out.println("No existen temperaturas que más se repitan");
    	else {
    		System.out.println("La(s) temperatura(s) que mas se repite(n):");
    		for(int i=0;i<r.length;i++)
    			System.out.printf("%6.2f, ",r[i]);
    		System.out.println();
    	}
    	System.out.printf("El promedio =%6.2f\n", obtienePromedio(t));
    }
    //*******************************************
    static void run() {
    	float temperaturas[]= new float[6];
    	leerTemp(temperaturas);
    	imprimeResultados(temperaturas);
    }
    //*******************************************
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        run();
	}

}
